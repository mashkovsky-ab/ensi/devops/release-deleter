const k8s = require('@kubernetes/client-node');
const shell = require('shelljs')

const k8sConfig = new k8s.KubeConfig();
k8sConfig.loadFromDefault();

const k8sApi = k8sConfig.makeApiClient(k8s.CustomObjectsApi);

let namespace = envOrFail('NAMESPACE');
let now = new Date();

function processMetaItems(items) {
    for (let item of items) {
        let releaseName = item.metadata.name,
            deleteAt = item.spec.deleteAt;
        if (!deleteAt) {
            continue;
        }
        let endOfLife = new Date(deleteAt);
        if (now > endOfLife) {
            console.debug(`Release '${releaseName}' is expired: {deleteAt: '${deleteAt}'}.`);
            deleteRelease(releaseName);
        }
    }
}

function deleteRelease(releaseName) {
    let command = `helm --namespace ${namespace} delete ${releaseName}`;
    console.debug(`Execute: '${command}'`);
    let result = shell.exec(command, {silent: true});
    if (result.code === 0) {
        console.debug('Success');
    } else {
        console.error(`Command '${command}' failed. Message:\n${result.stderr}`)
    }
}

function envOrFail(name) {
    let value = process.env[name];
    if (value === undefined || value === null || value === '') {
        fail(`${name} environment variable is required`);
    }
    return value;
}

function fail(message) {
    console.error(message);
    process.exit(1);
}

function logAxiosResponse(response) {
    console.error(`Request '${response.response.request.method} ${response.response.request.uri.href}' return bad response.\n` +
        `StatusCode: ${response.statusCode}\n` +
        `Body: ${JSON.stringify(response.body)}`);
}

k8sApi.listNamespacedCustomObject(
    'k8s.ensi.tech',
    'v1',
    namespace,
    'releasemetas'
).then(response => {
    if (response.body && response.body.items instanceof Array) {
        processMetaItems(response.body.items);
    } else {
        logAxiosResponse(response);
    }
}).catch(reason => {
    logAxiosResponse(reason);
});